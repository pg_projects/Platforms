package net.stawrul.services;

import net.stawrul.model.Book;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

import java.util.Objects;

import java.util.stream.Collectors;

/**
 * Komponent (serwis) biznesowy do realizacji operacji na książkach.
 */
@Service
public class BooksService extends EntityService<Book> {

    //Instancja klasy EntityManger zostanie dostarczona przez framework Spring
    //(wstrzykiwanie zależności przez konstruktor).
    public BooksService(EntityManager em) {

        //Book.class - klasa encyjna, na której będą wykonywane operacje
        //Book::getId - metoda klasy encyjnej do pobierania klucza głównego
        
        super(em, Book.class, Book::getId);
    }

    /**
     * Pobranie wszystkich książek z bazy danych.
     *
     * @param books
     * @return lista książek
     */
  
    public Integer getTotalPrice(List<Book> books) {
        if (books == null) return 0;
        
        int price = 0;
        for (Book bookUnit : books) {
            Integer unitPrice = em.find(Book.class, bookUnit.getId()).getUnitPrice();
            price = price + (unitPrice * bookUnit.getAmount());
        }
        
        return price;
    }
    public List<Book> findAll() {
        //pobranie listy wszystkich książek za pomocą zapytania nazwanego (ang. named query)
        //zapytanie jest zdefiniowane w klasie Book
        return em.createNamedQuery(Book.FIND_ALL, Book.class).getResultList();
    }
    public List<Book> findGreaterThenCount(int count)
    {
        return findAll().stream().filter(book -> book.getAmount()> count).collect(Collectors.toList());
    }
    
public List<Book> find(Integer unitPrice){
   return findAll().stream().filter(book -> book.getUnitPrice()>unitPrice).collect(Collectors.toList());
}
}
