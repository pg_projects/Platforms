/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stawrul;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import net.stawrul.model.Book;
import net.stawrul.services.BooksService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author YuriyKis
 */
@RunWith(MockitoJUnitRunner.class)
public class BooksServiceTest {
    
    @Mock
    EntityManager em;
    
    @Test
    public void validateTotalBookPrice() {
       
        List<Book> books = new ArrayList<Book>();
        
        Book book1 = new Book();
        book1.setAmount(20);
        book1.setUnitPrice(15);
        
        Book book2 = new Book();
        book2.setAmount(13);
        book2.setUnitPrice(150);
        
        Mockito.when(em.find(Book.class, book1.getId())).thenReturn(book1);
        Mockito.when(em.find(Book.class, book2.getId())).thenReturn(book2);
        
        books.add(book1);
        books.add(book2);
        
        BooksService bsvc = new BooksService(em);

       
        int result = bsvc.getTotalPrice(books);
        
        assertEquals(result, (20 * 15) + (13 * 150));
    }
    
    @Test
    public void validateBookAddition(){
      //   Arrange:
        List<Book> books = new ArrayList<Book>();
         
        Book book = new Book();
        book.setAmount(15);
        book.setUnitPrice(60);
        
        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);
        
        books.add(book);
        
        boolean result = books.isEmpty();        
        
        assertFalse(result);
        
    }
    
    @Test
    public void validateBookUpdate(){
         List<Book> books = new ArrayList<Book>();
         Book book = new Book();
        book.setAmount(15);
        book.setUnitPrice(60);
        book.setAuthor("Ktos");
            
         Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);
         
        books.add(book);
        
       String previousAuthor = book.getAuthor();
       book.setAuthor("KtosInny");
       
       assertNotSame(previousAuthor, book.getAuthor());
       
    }
}
